# SAE ESE Wifi BLE - Peripheral

Code pour la carte périphérique qui communique avec la carte centrale via Bluetooth.

### Matériel et logiciel requis
- Kit de prototypage PSoC : CY8CPROTO-063-BLE
- ModusToolbox 3.1

### Guide d'installation

1. Télécharger le dossier **peripheral** à l'emplacement souhaité.
2. Ouvrir Eclipse IDE for ModusToolbox et importer le projet en cliquant sur *Import existing application in place* dans le panneau en bas à gauche de l'application (Quick panel).
3. Connecter la carte PSoC via USB.
4. Lancer la compilation du code et la programmation de la carte en appuyant sur le bouton *Run* (si une boîte de dialogue apparaît, choisir *Program*). Si la programmation échoue, assurez-vous que le programmateur de la carte soit à jour : ouvrir l'application **Cypress Programmer**. À l'ouverture, l'application devrait avertir de l'obsolescence du programmateur. Cliquer sur le bouton *Upgrade Firmware* dans la boîte de dialogue qui apparaît.
5. La LED verte de la carte devrait clignoter, indiquant l'attente d'appairage bluetooth.
6. Une communication UART est possible pour vérifier le statut de la connexion bluetooth (par exemple via TeraTerm). Baud = 115200, flow control = Xon/Xoff.
7. Utiliser l'application AIROC Connect sur le smartphone pour se connecter à la carte.
   1. État de la LED utilisateur : Gatt DB -> Unknown Service -> Read & Write. Une valeur supérieure à zéro indique que la LED est allumée. Il est possible de lire et écrire cette valeur via le smartphone.
   2. Nombre d'appuis sur le bouton utilisateur : Gatt DB -> Unknown Service -> Read. C'est la valeur éponyme. Cette valeur nécessite l'appairage entre le smartphone et la carte, et ne peut pas être écrite via le smartphone.

### Travail à réaliser

- Concevoir et réaliser une carte contenant le capteur, la mise en forme du signal provenant du capteur, et la carte de prototypage PSoC.
- Acquérir et traiter la donnée provenant du capteur. La stocker dans la base de donnée GATT à chaque nouvelle lecture.
  - Dans un premier temps, la donnée est lue lors d'une requête de lecture provenant de la carte centrale. 
  - Dans un second temps, la donnée est mise à jour en continue. La carte centrale peut alors s'abonner aux notifications associées. 
